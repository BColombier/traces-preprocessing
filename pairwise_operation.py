# Imports for processing
import numpy as np
# from skimage.util.shape import view_as_windows

# Imports for parallel processing
from multiprocessing import Pool, current_process
import math
import copy

# Imports for script utilitaries
import logging as log
import argparse
import os

# Imports for dev
import cProfile
import time

def multi_run_wrapper(args):
    return pairwise_operation(*args)

def pairwise_operation(traces,
                       window_size,
                       minimum_distance,
                       operation,
                       dtype,
                       start_index=0,
                       record_pairs=False,
                       verbose=False,
                       first_chunk=True):

    """Operates in a sliding window_size over the trace
    Computes f(x_i, x_j) for all possible pairs of samples (x_i, x_j)
    in the window_size with distance(x_i, x_j) > minimum_distance.
    
    Keyword arguments:
    traces: numpy array holding the traces
    window_size: size of the window in which pairwise operation is done
    minimum_distance: minimum distance between two samples processed
    operation: processing operation to apply on the pair of samples
    start_index: 
    verbose: display INFO
    first_chunk: indicates first chunk of data for parallel processing

    Returns:
    preprocessed_trace: numpy array containing the preprocessed trace
    indexes: the indexes of the processed pairs
    """
    
    if verbose:
        log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
        if not current_process()._identity or current_process()._identity[0] == 1:
            log.info("Verbose output enabled")
    else:
        log.basicConfig(format="%(levelname)s: %(message)s")    
    
    nb_traces, nb_samples = np.shape(traces)
    if not current_process()._identity:
        log.info("Processing {0} traces of {1} samples".format(nb_traces, nb_samples))
            
    preprocessed_trace_length = 0
    with open('records_pairs_w{0}_d{1}.csv'.format(window_size, minimum_distance), 'w') as record_pairs_file:
        record_pairs_file.write("current_index, current_start_index, current_distance\n")
        for current_distance in range(minimum_distance, window_size):
            for current_start_index in range(nb_samples - current_distance):
                if first_chunk or (not first_chunk and current_start_index+current_distance>=window_size-1):
                    preprocessed_trace_length+=1
                    record_pairs_file.write("{0}, {1}, {2}\n".format(preprocessed_trace_length, current_start_index, current_distance))
    log.info("Original traces occupying {0} Mbytes".format(traces.nbytes/1000000))
    increase = preprocessed_trace_length/np.shape(traces)[1]
    log.info("Preprocessed traces will occupy {0}x more memory".format(increase))
    log.info("Preprocessed traces will occupy {0} Mbytes".format(increase*traces.nbytes/1000000))
    # Allocate memory for the preprocessed trace
    preprocessed_trace = np.zeros((preprocessed_trace_length, nb_traces), dtype=dtype)
    current_index = 0
    indexes = np.zeros((preprocessed_trace_length),dtype='i,i')
    average_trace = np.mean(traces, axis=0)
    # For all possible start indices in the window
    for current_distance in range(minimum_distance, window_size):
        print(current_distance)
        for current_start_index in range(nb_samples - current_distance):
            if first_chunk or (not first_chunk and current_start_index+current_distance>=window_size-1):
                value = np.array(operation(traces[:,current_start_index], traces[:,current_start_index+current_distance], average_trace[current_start_index], average_trace[current_start_index+current_distance]), ndmin=2)
                # Store the resulting vector
                preprocessed_trace[current_index,:] = np.transpose(value)[:,0]
                indexes[current_index] = (start_index+current_start_index, start_index+current_start_index+current_distance)
                # Increase the running index
                current_index+=1
    preprocessed_trace = np.transpose(preprocessed_trace)
    # return preprocessed_trace, indexes
    return preprocessed_trace

# Operations to perform on the pairs of samples
def multiplication(*args):
    return args[0] * args[1]
def addition(*args):
    return args[0] + args[1]
def squared_addition(*args):
    return (args[0] + args[1])*(args[0] + args[1])
def absolute_difference(*args):
    return abs(args[0] - args[1])
def centered_product(*args):
    return (args[0] - args[2]) * (args[1] - args[3])
    
if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("-o", "--op", type=str, choices=['addition', 'multiplication', 'squared_addition', 'absolute_difference', 'centered_product'])
    parser.add_argument("-w", "--window_size", type=int)
    parser.add_argument("-d", "--min_dist", type=int)
    parser.add_argument("-t", "--dtype", type=str, nargs='?', default='float64')
    parser.add_argument("-n", "--ncores", type=int)
    parser.add_argument("-r", "--record_pairs", action='store_true', default=False)
    parser.add_argument('-v', '--verbose', action='store_true')
    args = parser.parse_args()
    
    if args.op == 'multiplication': operation = multiplication
    elif args.op == 'addition': operation = addition
    elif args.op == 'squared_addition': operation = squared_addition
    elif args.op == 'absolute_difference': operation = absolute_difference
    elif args.op == 'centered_product': operation = centered_product
    dtype = np.dtype(args.dtype).type
    if args.verbose:
        log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
        if not current_process()._identity or current_process()._identity[0] == 1:
            log.info("Verbose output enabled")
    else:
        log.basicConfig(format="%(levelname)s: %(message)s")    

        
    # Generate fake data for testing purposes
    # fake_nb_samples = 1000
    # fake_nb_traces = 1000
    # test_array = np.random.rand(fake_nb_traces, fake_nb_samples)
    # traces = test_array
    # Load traces from file
    traces = np.load(args.traces_name)
    print("Input:", np.shape(traces))
    # Shorten the traces to split them into equally-sized chunks 
    shortened = 0
    while int(np.shape(traces)[1] + (args.ncores - 1)*(args.window_size - 1))%args.ncores != 0:
        traces = traces[:,:-1].copy()
        shortened+=1
    if shortened:
        log.warning("Traces shortened by {0} samples to split them into equally-sized chunks".format(shortened))
    nb_samples = int(np.shape(traces)[1])

    # Perform non-parallel preprocessing
    preprocessed_traces = []
    # preprocessed_traces, indexes = pairwise_operation(traces, args.window_size, args.min_dist, operation, dtype, args.verbose)

    # Init pool of workers for parallel preprocessing
    pool = Pool(args.ncores)
    # Compute the size of each chunk of traces to be preprocessed
    chunk_size = int(int(np.shape(traces)[1]+(args.ncores-1)*(args.window_size-1))/args.ncores)
    log.info("Traces split into {0} chunks of {1} samples".format(args.ncores, chunk_size))
    # Split the traces, with overlapping
    # if args.ncores > 1:
    #     traces = view_as_windows(traces, (np.shape(traces)[0],chunk_size), step=chunk_size-args.window_size+1)[0]
    # else:
    traces = [traces]
    # Create the list of arguments
    start_indexes = range(0, nb_samples, chunk_size-args.window_size+1)    
    arguments = [(trace_set, args.window_size, args.min_dist, operation, dtype, start_index, args.verbose, first_chunk) for (trace_set, start_index, first_chunk) in zip(traces, start_indexes, [True]+(args.ncores-1)*[False])]
    print arguments
    # Run the parallel computation
    # parallel_processing_results = np.concatenate(pool.map(multi_run_wrapper, arguments), axis=1)
    if args.ncores > 1:
        preprocessed_traces_parallel = np.concatenate(pool.map(multi_run_wrapper, arguments), axis=1)
    elif args.ncores == 1:
        preprocessed_traces_parallel = multi_run_wrapper(arguments[0])
    else:
        raise ValueError("You must have more than 0 cores")
    # print parallel_processing_results
    # preprocessed_traces_parallel, indexes_parallel = parallel_processing_results[::2], parallel_processing_results[1::2]
    # preprocessed_traces_parallel = np.concatenate(preprocessed_traces_parallel, axis=1)
    # indexes_parallel = np.concatenate(indexes_parallel, axis=1)
    
    # Compare normal and parallel processing
    # if preprocessed_traces:
    #     if np.all(preprocessed_traces.sort()==preprocessed_traces_parallel.sort()):
    #         if np.all(indexes.sort()==indexes_parallel.sort()):
    #             print "###\nGreat, sequential and\nparallel processing\nreturned the same result\n###"

    dirname, filename = os.path.split(args.traces_name)
    print("Final trace of dimensions:", np.shape(preprocessed_traces_parallel))
    np.save(os.path.join(dirname, "pairwise_"+args.op+"_"+filename), preprocessed_traces_parallel)
