import numpy as np

import argparse
import os

import matplotlib.pyplot as plt

def remove_window(traces, start_index, stop_index, plot_only=False):
    return np.delete(traces, range(start_index, stop_index), axis=1)

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("--start_index", type=int)
    parser.add_argument("--stop_index", type=int)
    parser.add_argument("--plot_only", type=bool)
    args = parser.parse_args()

    fake_nb_samples = 10
    fake_nb_traces = 2

    test_array = np.random.random_integers(10, size=(fake_nb_traces, fake_nb_samples))
    traces = test_array
    # Load traces from file
    traces = np.load(args.traces_name)
    traces_to_plot = traces[1:2,:]

    if args.plot_only:
        plt.plot(traces_to_plot[0,:], color='red', alpha=0.5)
        plt.plot(traces_to_plot[0,:args.start_index], color='green')
        plt.plot(range(args.stop_index, np.shape(traces_to_plot)[1]), traces_to_plot[0,args.stop_index:], color='green')
        plt.show()
    else:
        remove_windowed_traces = remove_window(traces, args.start_index, args.stop_index)

        dirname, filename = os.path.split(args.traces_name)
        filename, extension = os.path.splitext(filename)

        np.save(os.path.join(dirname, "remove_windowed_"+str(args.start_index)+"_"+str(args.stop_index)+"_"+filename+extension), remove_windowed_traces)
