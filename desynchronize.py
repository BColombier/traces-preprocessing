import numpy as np
import math

import argparse
import os

def desynchronize(traces, desynch):
    nb_traces = np.shape(traces)[0]
    desynch_amounts = np.random.binomial(desynch*2, 0.5, nb_traces)
    for i in range(len(traces)):
        start_pad = [traces[i,0]]*desynch_amounts[i]
        amount = desynch_amounts[i]
        print amount
        drop = np.shape(traces)[1] - amount
        traces[i,:] = np.hstack((start_pad, traces[i,:drop]))
    return traces

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("-d", "--desynch", type=int)
    args = parser.parse_args()

    # Load traces from file
    traces = np.load(args.traces_name)
    dirname, filename = os.path.split(args.traces_name)

    desynchronized_traces = desynchronize(traces, args.desynch)
    
    # np.save(os.path.join(dirname, "desynch_"+str(args.desynch)+"_"+filename), desynchronized_traces)
    np.save(os.path.join(dirname, "desynch_"+str(args.desynch)+"_"+filename), desynchronized_traces)
