import numpy as np
import math

import argparse
import os

# Possible operations
from pairwise_operation import *
from downsample import downsample
from filter_highest_variance import filter_highest_variance
from shorten import shorten

def group_process(dirname, filename, extension, prefix, nb_shares, function, *args):
    for share in [str(i) for i in range(nb_shares)]:
        traces = np.load(os.path.join(dirname, prefix+"_"+share+"_"+filename+extension))
        yield function(traces, *args)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("--prefix", type=str)
    parser.add_argument("--nb_shares", type=int)
    parser.add_argument("--function", type=str)

    group_process_args = parser.parse_known_args()[0]
    function_args = parser.parse_known_args()[1]
    functions={'pairwise_operation' : pairwise_operation,
               'downsample' : downsample,
               'filter_highest_variance' : filter_highest_variance,
               'shorten' : shorten,
           }
    try:
        function=functions[group_process_args.function]
    except KeyError:
        raise ValueError('Invalid function')

    print group_process_args
    print function_args

    dirname, filename = os.path.split(group_process_args.traces_name)
    filename, extension = os.path.splitext(filename)

    for counter, processed_trace in enumerate(group_process(dirname, filename, extension, group_process_args.prefix, group_process_args.nb_shares, function, *function_args)):
        print "Processing share {0}".format(counter)
        np.save(os.path.join(dirname, "processed_"+args.prefix+"_"+str(counter)+"_"+filename), processed_trace)
