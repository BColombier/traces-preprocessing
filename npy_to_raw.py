import numpy as np

import argparse
import os

def npy_to_raw(traces_name, new_type):
    traces = np.load(traces_name)

    print "Original type  :", traces.dtype
    print "New chosen type:", new_type
    dirname, filename = os.path.split(traces_name)
    filename, extension = os.path.splitext(filename)

    traces.astype(new_type).tofile(os.path.join(dirname, filename)+'.raw',
                                   sep = "",
                                   format = "%s")

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("-t", "--new_type", type=str)
    args = parser.parse_args()

    npy_to_raw(args.traces_name, args.new_type)
