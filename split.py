import numpy as np
import math

import argparse
import os

def split(traces, nb_shares):
    return np.split(traces, nb_shares)

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("-n", "--nb_shares", type=int)
    args = parser.parse_args()

    fake_nb_samples = 10
    fake_nb_traces = 12

    test_array = np.random.random_integers(10, size=(fake_nb_traces, fake_nb_samples))
    traces = test_array
    # Load traces from file
    traces = np.load(args.traces_name)

    split_traces = split(traces, args.nb_shares)

    dirname, filename = os.path.split(args.traces_name)

    for counter, traces in enumerate(split_traces):
        np.save(os.path.join(dirname, "split_"+str(counter)+"_"+filename), traces)
