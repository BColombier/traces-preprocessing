import numpy as np
import matplotlib.pyplot as plt

import argparse
import os

def add_correlated_noise_SNR(traces_names, SNR, ratio):
    nb_sets = len(traces_names)
    for traces_index, traces_name in enumerate(traces_names):
        noiseless_traces = np.load(traces_name)
        if traces_index == 0:
            nb_traces, nb_samples = np.shape(np.load(traces_name))
            noisy_traces = np.zeros((nb_traces, nb_samples, nb_sets))
        noisy_traces[...,traces_index] = np.load(traces_name)
    plt.subplot(2, 2, 1)
    plt.plot(noisy_traces[0,:,0])
    var_signal = np.std(noisy_traces)
    var_noise = var_signal*pow(10, (int(SNR/20)))
    noise = np.random.normal(0, var_noise, np.shape(noisy_traces))
    correlated_noise = np.multiply(noise, ratio)
    uncorrelated_noise = np.multiply(noise, 1-ratio)
    # Remove last dimension
    correlated_noise = correlated_noise[:,:,0]
    # Add one dimension
    correlated_noise = correlated_noise[...,np.newaxis]
    # Duplicate along the new dimension
    correlated_noise = np.repeat(correlated_noise, nb_sets, axis=2)
    noisy_traces = noisy_traces+correlated_noise+uncorrelated_noise
    return noisy_traces

if __name__ == "__main__":

    def restricted_ratio(x):
        x = float(x)
        if x < 0.0 or x > 1.0:
            raise argparse.ArgumentTypeError("Ratio {} not in range [0.0, 1.0]".format(x))
        return x
    
    # Parsing arguments
    parser = argparse.ArgumentParser(description='Add correlated noise to multiple sets of traces, assuming there was no noise in the first place')
    parser.add_argument("traces_names", nargs='+', type=str)
    parser.add_argument("-n", "--SNR", type=int)
    parser.add_argument("-r", "--ratio", type=restricted_ratio)
    args = parser.parse_args()

    noisy_traces = add_correlated_noise_SNR(args.traces_names, args.SNR, args.ratio)

    for i in range(noisy_traces.shape[-1]):

        dirname, filename = os.path.split(args.traces_names[i])
        filename, extension = os.path.splitext(filename)

        np.save(os.path.join(dirname, "noisy_SNR{}_ratio{}_{}".format(args.SNR, args.ratio, filename)), noisy_traces[...,i])
