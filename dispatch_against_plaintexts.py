import numpy as np

import argparse
import os

def dispatch_against_plaintexts(plaintexts, ref_plaintext):
    print "Plaintexts:", np.shape(plaintexts)
    ref_plaintext = ref_plaintext.replace('0x', '')
    ref_plaintext = [ref_plaintext[i:i+2] for i in range(0, len(ref_plaintext), 2)]
    ref_plaintext = np.array([int(hex_val, 16) for hex_val in ref_plaintext])
    print "Ref ", ref_plaintext
    print "======================"
    for i in range(15):
        print plaintexts[i]
    # int_array_ref_plaintext =
    indexes_match_plaintext = np.equal(plaintexts, ref_plaintext[None,:])[:,0]
    indexes_non_match_plaintext = np.not_equal(plaintexts, ref_plaintext[None,:])[:,0]
    indexes_match_plaintext = np.where(indexes_match_plaintext == True)[0]
    indexes_non_match_plaintext = np.where(indexes_non_match_plaintext == True)[0]
    return indexes_match_plaintext, indexes_non_match_plaintext

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Dispatch against plaintexts')
    parser.add_argument("plaintexts_name", type=str)
    parser.add_argument("-r", "--ref_plaintext", type=str, default='0xda39a3ee5e6b4b0d3255bfef95601890')
    args = parser.parse_args()

    plaintexts = np.load(args.plaintexts_name)

    indexes_match_plaintext, indexes_non_match_plaintext = dispatch_against_plaintexts(plaintexts, args.ref_plaintext)

    dirname, filename = os.path.split(args.plaintexts_name)
    filename, extension = os.path.splitext(filename)

    np.savetxt(os.path.join(dirname, filename+".output_0"), indexes_non_match_plaintext, fmt='%0i')
    np.savetxt(os.path.join(dirname, filename+".output_1"), indexes_match_plaintext, fmt='%0i')
