import numpy as np

import argparse
import os

def scale_float_to_uinttype(traces, uinttype='uint8'):
    if uinttype not in ['uint8', 'uint16', 'uint32', 'uint64']:
        raise TypeError ('Cannot rescale to this type, choose an unsigned integer (uintXX) type instead')
    new_max = np.iinfo(uinttype).max
    new_min = np.iinfo(uinttype).min    
    prev_max = np.amax(traces)
    prev_min = np.amin(traces)
    traces -= prev_min
    offset_max = np.amax(traces)
    scale_factor = float(new_max)/offset_max
    traces *= scale_factor
    # print "Achievable new min :", np.iinfo(uinttype).min
    # print "Achievable new max :", np.iinfo(uinttype).max
    # print "Actual new min     :", np.amin(scaled_traces)
    # print "Actual new max     :", np.amax(scaled_traces)
    return np.around(traces).astype(uinttype)

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("--uinttype", type=str, default='uint8')
    args = parser.parse_args()

    traces = np.load(args.traces_name)

    scaled_traces = scale_float_to_uinttype(traces, args.uinttype)

    dirname, filename = os.path.split(args.traces_name)
    filename, extension = os.path.splitext(filename)

    np.save(os.path.join(dirname, filename+"_as_"+args.uinttype+extension), scaled_traces)
