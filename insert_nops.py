import numpy as np
import math
import random

import argparse
import os

def insert_nops(traces, number, width, beginning, end, log):
    nb_traces, nb_samples = np.shape(traces)
    noped_traces = np.zeros((nb_traces, nb_samples+(number*width)))
    if end == -1:
        end = nb_samples
    for trace in range(nb_traces):
        # Pick nop ampitudes between -0.03 and 0.02
        nop_amp = (5*np.random.random_sample((number*width))-3)/100
        # Choose positions of insertion for the nops
        pos = random.sample(range(beginning, end), number)
        # Multiply the positions of insertion by width
        pos = np.repeat(pos, width, 0)
        # Insert the nops at the positions
        noped_traces[trace] = np.insert(traces[trace], pos, nop_amp)
        if log:
            with open("log_nop_n{0}_w{1}.txt".format(number, width), "a") as logfile:
                for i in np.unique(pos):
                    print i
                    logfile.write(str(int(i))+",")
                logfile.write(str(width)+"\n")
    print np.shape(noped_traces)
    return noped_traces

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("-n", "--number", type=int)
    parser.add_argument("-w", "--width", type=int)
    parser.add_argument("-b", "--beginning", type=int, nargs='?', default=0)
    parser.add_argument("-e", "--end", type=int, nargs='?', default=-1)
    parser.add_argument("-l", "--log", action='store_true')
    args = parser.parse_args()

    # Load traces from file
    traces = np.load(args.traces_name)
    dirname, filename = os.path.split(args.traces_name)

    noped_traces = insert_nops(traces,
                               args.number,
                               args.width,
                               args.beginning,
                               args.end,
                               args.log)
    # np.save(os.path.join(dirname, "desynch_"+str(args.desynch)+"_"+filename), desynchronized_traces)
    np.save(os.path.join(dirname, "noped_n{0}_w{1}_{2}.npy".format(args.number, args.width, filename)), noped_traces)
