import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats

import argparse

import warnings
warnings.filterwarnings('error')

def generate_dummy_data(n_traces, n_samples):

    vals = np.random.randint(0, 256, n_traces)
    traces = np.random.normal(0, 1, (n_traces, n_samples))
    traces[:,5]+=0.1*vals
    return (traces, vals)


def find_extreme_f_values(f_statistics):
    d = np.abs(f_statistics - np.median(f_statistics))
    mdev = np.median(d)
    s = d/mdev if mdev else 0.
    extreme_vals = f_statistics[s>7]
    return np.where(np.isin(f_statistics, extreme_vals) == True)[0]


def f_test(traces, intermediate_values, highlight):

    nb_traces, nb_samples = np.shape(traces)
    f_statistics = np.zeros(nb_samples)
    int_val_clusters = [[index for (index, val) in enumerate(intermediate_values) if (val == target)] for target in range(256)]
    for sample_index, samples in enumerate(traces.T):
        if sample_index%(nb_samples/10)==0:
            print("{:>2}%".format(int(100*sample_index/nb_samples)))
        samples_clusters = [samples[int_val_cluster] for int_val_cluster in int_val_clusters]
        try:
            f_statistic, p_value = stats.f_oneway(*samples_clusters)
        except RuntimeWarning:
            raise ValueError("Cluster(s) of intermediate values #{} are empty:".format(int_val_clusters.index([])))
        f_statistics[sample_index] = f_statistic
    if highlight:
        plt.plot(np.average(traces, axis=0))
        for extreme_val in find_extreme_f_values(f_statistics):
            plt.axvspan(extreme_val-1, extreme_val+1, color='red', alpha=0.5)
    else:
        plt.plot(f_statistics, "o", markersize=2, color='red')
    plt.show()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Compute F-test on a set of traces')
    parser.add_argument("traces_names", type=str, help="name of the traces file")
    parser.add_argument("intermediate_values", type=str, help="name of the intermediate values file")
    parser.add_argument("subset", type=int, nargs='?', default=0, help="size of the subset of traces to consider")
    parser.add_argument("-hi", "--highlight", action="store_true", help="highlight leakage on traces")
    args = parser.parse_args()

    # traces, int_val = generate_dummy_data(20000, 30)
    traces = np.load(args.traces_names)
    int_val = np.load(args.intermediate_values)
    if args.subset:
        traces = traces[:args.subset,:]
        int_val = int_val[:args.subset]
    sca_f_test(traces, int_val, args.highlight)
