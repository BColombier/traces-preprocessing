import numpy as np

import argparse
import os

def shorten(traces, start_index, stop_index):
    return traces[:,start_index:stop_index]

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("--start_index", type=int, default=0)
    parser.add_argument("--stop_index", type=int)
    args = parser.parse_args()

    fake_nb_samples = 10
    fake_nb_traces = 2

    test_array = np.random.random_integers(10, size=(fake_nb_traces, fake_nb_samples))
    # traces = test_array
    # Load traces from file
    traces = np.load(args.traces_name)

    shortened_traces = shorten(traces, args.start_index, args.stop_index)

    dirname, filename = os.path.split(args.traces_name)
    filename, extension = os.path.splitext(filename)

    np.save(os.path.join(dirname, "shortened_"+str(args.start_index)+"_"+str(args.stop_index)+"_"+filename+extension), shortened_traces)
