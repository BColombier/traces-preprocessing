import numpy as np
import argparse
import os

import matplotlib.pyplot as plt

def realign(traces_file, ref_trace_index=0):
    traces = np.load(traces_file)
    realigned_traces = np.zeros(np.shape(traces))
    ref_trace = traces[ref_trace_index]
    print np.shape(ref_trace)
    nb_traces = np.shape(traces)[0]
    nb_samples = np.shape(traces)[1]
    realigned_traces[0] = ref_trace
    for i in range(nb_traces):
        if i != ref_trace_index:
            shift = np.argmax(np.correlate(ref_trace, traces[i], mode="full")) - nb_samples
            realigned_traces[i] = np.roll(traces[i,:], shift)
    dirname, filename = os.path.split(args.traces_file)
    np.save(os.path.join(dirname, "realigned_"+filename), realigned_traces)

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_file", type=str)
    parser.add_argument("-r", "--ref_trace_index", type=int, default=0)
    args = parser.parse_args()
    realign(args.traces_file, args.ref_trace_index)
