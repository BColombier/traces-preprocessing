import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import argparse

def plot(traces_file, nb_subplots=1):
    traces = np.load(traces_file)
    # points_of_interest = [(21, 66),
    #                       (35, 66),
    #                       (49, 66),
    #                       (63, 66),
    #                       (167, 66),
    #                       (181, 66),
    #                       (195, 66),
    #                       (153, 72)]
    points_of_interest = []
    colors = {66:'#1250D2',
              72:'#C21637'}
    if nb_subplots>1:
        f, axarr = plt.subplots(nb_subplots, sharex=True)    
        for i in range(nb_subplots):
            axarr[i].plot((traces[i,:]), color='#666666')
    elif nb_subplots == 1:
        plt.plot(traces[0,:], zorder=-1, color='#666666', linewidth = 0.5)
        plt.scatter([poi[0] for poi in points_of_interest], [traces[0,poi[0]] for poi in points_of_interest], color='green', zorder=1)
        plt.scatter([poi[0]+poi[1] for poi in points_of_interest], [traces[0,poi[0]+poi[1]] for poi in points_of_interest], color='red', zorder=1)        
        for poi in points_of_interest:
            a = patches.FancyArrowPatch((poi[0],traces[0,poi[0]]), (poi[0]+poi[1],traces[0,poi[0]+poi[1]]), connectionstyle="arc3,rad=1", color=colors[poi[1]], arrowstyle=patches.ArrowStyle("Fancy, head_length=6, head_width=6, tail_width=1"))
            plt.gca().add_patch(a)
    plt.show()

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_file", type=str)
    parser.add_argument("-n", "--nb_subplots", type=int, default=1)
    args = parser.parse_args()
    plot(args.traces_file, args.nb_subplots)
