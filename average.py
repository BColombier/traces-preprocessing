import numpy as np
import argparse
import os

import matplotlib.pyplot as plt

def realign(traces_file):
    traces = np.load(traces_file)
    averaged_traces = np.mean(traces, axis=0)
    print(np.shape(averaged_traces))
    dirname, filename = os.path.split(args.traces_file)
    np.save(os.path.join(dirname, "averaged_"+filename), averaged_traces[None,:])

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_file", type=str)
    args = parser.parse_args()
    realign(args.traces_file)
