import numpy as np

import argparse
import os

def npy_to_bin(traces_name, output_format):
    traces = np.load(traces_name)

    dirname, filename = os.path.split(traces_name)
    filename, extension = os.path.splitext(filename)

    traces.astype(output_format).tofile(os.path.join(dirname, filename)+".bin")

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("-o", "--output_format", type=str)
    args = parser.parse_args()

    npy_to_bin(args.traces_name, args.output_format)
