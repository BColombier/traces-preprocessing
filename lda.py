import numpy as np
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.externals import joblib

import argparse
import os

def lda(traces_name, keys_name, nb_components, target_key_byte, save_model = False, load_model = False, model = "./model.pkl"):
    print "Loading data"
    traces = np.load(traces_name)
    keys = np.load(keys_name)
    
    dirname, filename = os.path.split(traces_name)
    filename, extension = os.path.splitext(filename)

    if load_model:
        print "Loading model"
        model = joblib.load(model)
    else:
        print "Building LDA object"
        lda = LinearDiscriminantAnalysis(n_components=nb_components)
        print "Fitting"
        model = lda.fit(traces, keys[:,target_key_byte])
    lda_traces = model.transform(traces)
    np.save(os.path.join(dirname, "lda_"+str(nb_components)+"_"+filename)+'.npy', lda_traces)
    if save_model:
        joblib.dump(model, os.path.join(dirname, "model_lda_"+str(nb_components)+"_"+filename)+'.pkl')
            
if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("keys_name", type=str)
    parser.add_argument("-n", "--nb_components", type=int)
    parser.add_argument("-b", "--target_key_byte", type=int)
    parser.add_argument("-s", "--save_model", action='store_true', default=False)
    parser.add_argument("-l", "--load_model", action='store_true', default=False)
    parser.add_argument("-m", "--model", type=str, default="./model.pkl")
    args = parser.parse_args()

    lda(args.traces_name, args.keys_name, args.nb_components, args.target_key_byte, args.save_model, args.load_model, args.model)
