import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import argparse
from numpy import genfromtxt
import os

def plot(csv_file, original_trace_file, threshold):
    csv = np.genfromtxt(csv_file)
    print np.shape(csv)
    ax = plt.gca()
    plt.yscale('symlog')
    minor_ticks = range(-100, 0, 10)+range(-9, 0, 1)+[0]+range(1, 10, 1)+range(10, 110, 10)
    print minor_ticks
    ax.set_yticks(minor_ticks, minor=True)
    plt.ylabel("t-statistic")
    plt.grid(b=True, which='minor', color='#C0C0C0', linestyle='--', zorder=0)
    plt.grid(b=True, which='major', color='k', linestyle='-', zorder=1)    
    plt.xlim(0, np.shape(csv)[0])
    plt.plot(csv, zorder=5, color='#666666')
    plt.plot(np.shape(csv)[0]*[4.5], color='red', zorder=10)
    plt.plot(np.shape(csv)[0]*[-4.5], color='red', zorder=10)
    dirname, filename = os.path.split(csv_file)
    filename, extension = os.path.splitext(filename)
    plt.savefig(os.path.join(dirname, "t-statistic.png"))
    plt.show()
    plt.close()
    plt.xlim(0, np.shape(csv)[0])    
    pois = []
    for index, t_val in enumerate(csv):
        if abs(t_val) > threshold:
            pois.append(index)
    traces = np.load(original_trace_file)
    plt.plot(traces[0], linewidth = 0.5)
    plt.scatter(pois, traces[0,pois], marker = 'x', color = 'red', zorder=5)
    plt.savefig(os.path.join(dirname, "highlight_"+str(threshold)+"_on_trace.png"))
    plt.show()
    plt.close()

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("csv_file", type=str)
    parser.add_argument("original_trace_file", type=str)
    parser.add_argument("-t", "--threshold", type=float)
    args = parser.parse_args()
    plot(args.csv_file, args.original_trace_file, args.threshold)
