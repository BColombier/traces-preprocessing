import numpy as np

import argparse
import os

def contiguify(traces_names):
    for traces_name in traces_names:
        print "Processing file {0}".format(traces_name)
        traces = np.load(traces_name)

        dirname, filename = os.path.split(traces_name)
        filename, extension = os.path.splitext(filename)
    
        np.save(os.path.join(dirname, "contiguous_"+filename)+'.npy', np.ascontiguousarray(traces))

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_names", type=str, nargs='+')
    args = parser.parse_args()

    contiguify(args.traces_names)
