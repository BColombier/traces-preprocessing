import numpy as np
import math

import argparse
import os

def merge(*arg):
    arg = arg[0] # Get first tuple element
    print arg
    final_trace = np.vstack([np.load(i) for i in arg])
    print np.shape(final_trace)
    return final_trace

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces", nargs='*', type=str)
    args = parser.parse_args()

    dirname, filename = os.path.split(args.traces[0])
    filename, extension = os.path.splitext(filename)
    
    merged_traces = merge(args.traces)

    np.save(os.path.join(dirname, "merged_"+filename), merged_traces)
