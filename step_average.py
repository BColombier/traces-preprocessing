import numpy as np

import argparse
import os
import logging as log

def step_average(traces, step_size, offset=0):
    
    shortened = 0
    traces = traces[:,offset:]
    while np.shape(traces)[1]%step_size!=0:
        traces = traces[:,:-1].copy()        
        shortened+=1
    if shortened:
        log.warning("Traces shortened by {0} samples to split them into equally-sized chunks".format(shortened))
    
    step_traces = np.split(traces, np.shape(traces)[1]/step_size, axis=1)
    step_averaged_traces = np.transpose(np.zeros(np.shape(step_traces)[0:2]))
    for index, i in enumerate(step_traces):
        mean = np.mean(step_traces[index], axis=1)
        step_averaged_traces[:,index] = mean
    return step_averaged_traces

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("--step_size", type=int)
    parser.add_argument("--offset", type=int, nargs='?', default=0)
    args = parser.parse_args()

    fake_nb_samples = 8
    fake_nb_traces = 2

    test_array = np.random.random_integers(2, size=(fake_nb_traces, fake_nb_samples))
    traces = test_array
    # Load traces from file
    traces = np.load(args.traces_name)

    step_averaged_traces = step_average(traces, args.step_size, args.offset)
    dirname, filename = os.path.split(args.traces_name)
    filename, extension = os.path.splitext(filename)

    np.save(os.path.join(dirname, filename+"_step_averaged"+extension), step_averaged_traces)
