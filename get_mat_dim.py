import numpy as np

import argparse
import os

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    args = parser.parse_args()

    traces = np.load(args.traces_name)
    print("Matrix of {0} of size {1}".format(traces.dtype, np.shape(traces)))
