import numpy as np

import argparse
import os

def power(signal):
    return np.mean(np.sum(np.multiply(signal, signal), axis = 1))

def add_noise(traces, variance):
    nb_traces, nb_samples = np.shape(traces)
    
    noise = np.random.normal(0, variance, np.shape(traces))
    mean_trace = np.tile(np.mean(traces, axis = 0), (nb_traces, 1))
    print np.shape(mean_trace)

    print np.shape(power(traces))
    print "  P(noisy traces) :", 10*np.log10(power(traces))
    print "  P(clean traces) :", 10*np.log10(power(mean_trace))
    print "P(included noise) :", 10*np.log10(power(traces - mean_trace))
    print "   P(added noise) :", 10*np.log10(power(noise))
    print "              SNR :", 10*np.log10(power(mean_trace)/power(noise))
    return traces+noise

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("-v", "--variance", type=float)
    args = parser.parse_args()

    traces = np.load(args.traces_name)

    noisy_traces = add_noise(traces, args.variance)

    dirname, filename = os.path.split(args.traces_name)
    filename, extension = os.path.splitext(filename)

    np.save(os.path.join(dirname, "noisy_"+str(args.variance)+"_"+filename), noisy_traces)
