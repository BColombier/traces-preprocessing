import numpy as np
from sklearn.manifold import SpectralEmbedding

import argparse
import os

def le(traces_name, nb_components, nb_cores, k_neighbors):
    print "Loading data"
    traces = np.load(traces_name)
    
    dirname, filename = os.path.split(traces_name)
    filename, extension = os.path.splitext(filename)

    print "Performing the embedding"
    le = SpectralEmbedding(n_components=nb_components, n_jobs=nb_cores, n_neighbors=k_neighbors)
    le_traces = le.fit_transform(traces)
    print "Saving data"
    np.save(os.path.join(dirname, "le_"+str(nb_components)+"_"+filename)+'.npy', le_traces)
            
if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("-n", "--nb_components", type=int)
    parser.add_argument("-c", "--nb_cores", type=int, default=1)
    parser.add_argument("-k", "--k_neighbors", type=int)
    args = parser.parse_args()

    le(args.traces_name, args.nb_components, args.nb_cores, args.k_neighbors)
