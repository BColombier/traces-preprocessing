import numpy as np

import argparse
import os

def duplicate(traces, number):
    return np.repeat(traces, number, axis=0)

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("-n", "--number", type=int)
    args = parser.parse_args()

    traces = np.load(args.traces_name)

    duplicated_traces = duplicate(traces, args.number)

    dirname, filename = os.path.split(args.traces_name)
    filename, extension = os.path.splitext(filename)

    np.save(os.path.join(dirname, "dup_"+str(args.number)+"_"+filename), duplicated_traces)
