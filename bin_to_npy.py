import numpy as np

import argparse
import os

def bin_to_npy(traces_name, input_format, nb_rows, nb_columns):
    traces = np.fromfile(traces_name, dtype=input_format)
    if not nb_columns:
        nb_columns = int(np.shape(traces)[0]/nb_rows)
    traces = np.reshape(traces, (nb_rows, nb_columns))
    print(np.shape(traces))
    dirname, filename = os.path.split(traces_name)
    filename, extension = os.path.splitext(filename)

    print(os.path.join(dirname, filename)+".npy")
    np.save(os.path.join(dirname, filename)+".npy", traces)

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("-i", "--input_format", type=str)
    parser.add_argument("-r", "--nb_rows", type=int)
    parser.add_argument("-c", "--nb_columns", type=int, default=False)
    args = parser.parse_args()

    bin_to_npy(args.traces_name, args.input_format, args.nb_rows, args.nb_columns)
