import numpy as np
from sklearn.decomposition import PCA
from sklearn.externals import joblib

import argparse
import os

def pca(traces_name, nb_components, save_model = False, load_model = False, model = "./model.pkl"):
    print "Loading data"
    traces = np.load(traces_name)
    
    dirname, filename = os.path.split(traces_name)
    filename, extension = os.path.splitext(filename)

    if load_model:
        print "Loading model"
        model = joblib.load(model)
    else:
        print "Building PCA object"
        pca = PCA(n_components=nb_components)
        print "Fitting"
        model = pca.fit(traces)
    pca_traces = model.transform(traces)
    np.save(os.path.join(dirname, "pca_"+str(nb_components)+"_"+filename)+'.npy', pca_traces)
    if save_model:
        joblib.dump(model, os.path.join(dirname, "model_pca_"+str(nb_components)+"_"+filename)+'.pkl')
            
if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    parser.add_argument("-n", "--nb_components", type=int)
    parser.add_argument("-s", "--save_model", action='store_true', default=False)
    parser.add_argument("-l", "--load_model", action='store_true', default=False)
    parser.add_argument("-m", "--model", type=str, default="./model.pkl")
    args = parser.parse_args()

    pca(args.traces_name, args.nb_components, args.save_model, args.load_model, args.model)
