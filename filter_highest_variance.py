import numpy as np

import argparse
import os

def filter_highest_variance(traces, ratio=0, nsamples=0):

    """
    Extracts the samples with the highest variance along with their indices 
    
    Keyword arguments:
    traces: numpy array holding the traces
    ratio: ratio of samples to keep

    Returns:
    traces: numpy array containing the filtered trace
    indexes: the indexes of the samples of interest
    """

    if ratio:
        nb_to_keep = np.shape(traces)[1]*ratio
    elif nsamples:
        nb_to_keep = nsamples
    to_keep = np.argpartition(np.var(traces, axis=0), -nb_to_keep)[-nb_to_keep:]
    permutation = to_keep.argsort()
    return traces[:,to_keep[permutation]], to_keep[permutation]

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess traces')
    parser.add_argument("traces_name", type=str)
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--ratio", type=float)
    group.add_argument("--nsamples", type=int)
    args = parser.parse_args()

    traces = np.load(args.traces_name)

    dirname, filename = os.path.split(args.traces_name)
    filename, extension = os.path.splitext(filename)
    if args.ratio:
        filtered_variance_traces = filter_highest_variance(traces, ratio=args.ratio)
        np.save(os.path.join(dirname, filename+"_filtered_highest_variance_ratio_"+str(args.ratio)+extension), filtered_variance_traces)
    elif args.nsamples:
        filtered_variance_traces = filter_highest_variance(traces, nsamples=args.nsamples)
        np.save(os.path.join(dirname, filename+"_filtered_highest_variance_nsamples_"+str(args.nsamples)+extension), filtered_variance_traces)

