import numpy as np

import argparse
import os

def npy_to_hex(plaintexts_name):
    plaintexts = np.load(plaintexts_name)

    print plaintexts.dtype

    dirname, filename = os.path.split(plaintexts_name)
    filename, extension = os.path.splitext(filename)

    with open(os.path.join(dirname, filename+".hex"), 'w') as target_file:
        if len(np.shape(plaintexts)) == 1: # There is only one plaintext
                first_byte = '0x'+hex(plaintexts[0])[2:].zfill(2).upper()
                other_bytes = ''.join([hex_val[2:].zfill(2).upper() for hex_val in map(hex, plaintexts)[1:]])
                target_file.write(first_byte+other_bytes+"\n")
        elif len(np.shape(plaintexts)) == 2: # There are multiple plaintexts
            for plaintext in plaintexts:
                first_byte = '0x'+hex(plaintext[0])[2:].zfill(2).upper()
                other_bytes = ''.join([hex_val[2:].zfill(2).upper() for hex_val in map(hex, plaintext)[1:]])
                target_file.write(first_byte+other_bytes+"\n")
        else:
            raise ValueError("Invalid file, cannot handle such dimansions")

if __name__ == "__main__":

    # Parsing arguments
    parser = argparse.ArgumentParser(description='Preprocess plaintexts')
    parser.add_argument("plaintexts_name", type=str)
    args = parser.parse_args()

    npy_to_hex(args.plaintexts_name)
